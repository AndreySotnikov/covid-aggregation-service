# COVID confirmed cases aggregation service

This service provides API to get aggregated confirmed cases statistics (min/max) for specified countries on dates range.

The service uses https://api.covid19api.com API as a datasource.

## Usage

Available URL paths:

| Path                        | Description                                                                                                             |
|-----------------------------|-------------------------------------------------------------------------------------------------------------------------|
| /api/v1/covid/confirmed/min | Calculates the number of minimal confirmed cases, the country where it occurs, and the date in the specific date range. |
| /api/v1/covid/confirmed/max | Calculates the number of maximum confirmed cases, the country where it occurs, and the date in the specific date range. |

The following query parameters are supported by each path:

| Parameter | Is Required | Description                                                                                                                                   |
|-----------|-------------|-----------------------------------------------------------------------------------------------------------------------------------------------|
| countries | true        | List of country names separated by commas. Each country name should be in ISO Alpha-2 format. (https://www.iban.com/country-codes). e.g. "RU" | 
| dateFrom  | true        | The start date for the search is in ISO format. e.g. "2020-01-01"                                                                             | 
| dateTo    | true        | The end date for the search is in ISO format. e.g. "2020-01-01"                                                                               |

