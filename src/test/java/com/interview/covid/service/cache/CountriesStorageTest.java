package com.interview.covid.service.cache;

import com.interview.covid.exception.UnknownCountryException;
import com.interview.covid.external.CovidDataProvider;
import com.interview.covid.external.model.Country;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
public class CountriesStorageTest {

    @Autowired
    private CountriesStorage countriesStorage;

    @MockBean
    private CovidDataProvider covidDataProvider;


    @BeforeEach
    public void initTest() {
        when(covidDataProvider.findSupportedCountries())
                .thenReturn(List.of(
                        new Country("russia", "RU"),
                        new Country("united-states", "US"),
                        new Country("armenia", "AM")
                ));
        countriesStorage.refreshData();
    }

    @Test
    public void whenKeyExistsThenReturnData() {
        var countryByApiCode = countriesStorage.getCountryByApiCode("russia");
        assertNotNull(countryByApiCode);
        assertEquals("RU", countryByApiCode.iso2Code());
        assertEquals("russia", countryByApiCode.apiCode());

        var countryByIso2Code = countriesStorage.getCountryByIso2Code("RU");
        assertNotNull(countryByIso2Code);
        assertEquals("RU", countryByIso2Code.iso2Code());
        assertEquals("russia", countryByIso2Code.apiCode());
    }

    @Test
    public void whenKeyDoesntExistsThenException() {
        assertThrowsExactly(UnknownCountryException.class, () -> countriesStorage.getCountryByApiCode("q-q"));
        assertThrowsExactly(UnknownCountryException.class, () -> countriesStorage.getCountryByIso2Code("QQ"));
    }

}
