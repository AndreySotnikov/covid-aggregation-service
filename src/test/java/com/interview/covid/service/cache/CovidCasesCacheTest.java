package com.interview.covid.service.cache;

import com.interview.covid.exception.ExternalApiException;
import com.interview.covid.exception.TooManyApiRequestsException;
import com.interview.covid.external.CovidDataProvider;
import com.interview.covid.external.model.CountryCases;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;

import static com.interview.covid.util.TestUtils.generateTestData;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;
import static org.mockito.Mockito.*;

@SpringBootTest
public class CovidCasesCacheTest {

    @Autowired
    private CovidCasesCache covidCasesCache;

    @MockBean
    private CovidDataProvider covidDataProvider;

    @Test
    public void whenRequestSameCountryThenLoadFromCache() {
        when(covidDataProvider.findConfirmedCasesByCountry("russia"))
                .thenReturn(new CountryCases("russia", List.of(
                        generateTestData(10L, 2022, 1, 1),
                        generateTestData(15L, 2022, 1, 2),
                        generateTestData(20L, 2022, 1, 3)
                )));

        covidCasesCache.findCachedConfirmedCasesByCountry("russia");
        covidCasesCache.findCachedConfirmedCasesByCountry("russia");

        verify(covidDataProvider, times(1)).findConfirmedCasesByCountry("russia");
    }

    @Test
    public void whenUnableToLoadDataThenException() {
        when(covidDataProvider.findConfirmedCasesByCountry("united-states"))
                .thenThrow(new TooManyApiRequestsException("Too Many Requests"));

        assertThrowsExactly(ExternalApiException.class,
                () -> covidCasesCache.findCachedConfirmedCasesByCountry("united-states"));
    }

}
