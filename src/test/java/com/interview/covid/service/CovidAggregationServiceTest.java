package com.interview.covid.service;

import com.interview.covid.exception.NoDataException;
import com.interview.covid.external.model.Country;
import com.interview.covid.external.model.CountryCases;
import com.interview.covid.service.cache.CountriesStorage;
import com.interview.covid.service.cache.CovidCasesCache;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import static com.interview.covid.util.TestUtils.generateTestData;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrowsExactly;
import static org.mockito.Mockito.when;

@SpringBootTest
public class CovidAggregationServiceTest {

    @MockBean
    private CovidCasesCache covidCasesCache;
    @MockBean
    private CountriesStorage countriesStorage;
    @Autowired
    private CovidAggregationService covidAggregationService;

    @BeforeEach
    public void beforeTest() {
        when(countriesStorage.getCountryByIso2Code("RU")).thenReturn(new Country("russia", "RU"));
        when(countriesStorage.getCountryByApiCode("russia")).thenReturn(new Country("russia", "RU"));

        when(covidCasesCache.findCachedConfirmedCasesByCountry("russia"))
                .thenReturn(new CountryCases("russia", List.of(
                        generateTestData(10L, 2022, 1, 1),
                        generateTestData(12L, 2022, 1, 2),
                        generateTestData(14L, 2022, 1, 3),
                        generateTestData(2L, 2022, 1, 4),
                        generateTestData(23L, 2022, 1, 5))
                ));
    }

    @Test
    public void whenSourceDateRangeIsLessThanStorageThenFilterDataAndCalculateMin() {
        var dateFrom = LocalDate.of(2022, 1, 2);
        var dateTo = LocalDate.of(2022, 1, 3);
        var result = covidAggregationService.calculateMinMax("min", Collections.singletonList("RU"), dateFrom, dateTo);
        assertEquals(12L, result.count());
        assertEquals(LocalDate.of(2022, 1, 2), result.date());
        assertEquals("RU", result.country());
    }

    @Test
    public void whenSourceDateRangeIsLessThanStorageThenFilterDataAndCalculateMax() {
        var dateFrom = LocalDate.of(2022, 1, 2);
        var dateTo = LocalDate.of(2022, 1, 3);
        var result = covidAggregationService.calculateMinMax("max", Collections.singletonList("RU"), dateFrom, dateTo);
        assertEquals(14L, result.count());
        assertEquals(LocalDate.of(2022, 1, 3), result.date());
        assertEquals("RU", result.country());
    }

    @Test
    public void whenOperationIsUnknownThenException() {
        var dateFrom = LocalDate.of(2022, 1, 2);
        var dateTo = LocalDate.of(2022, 1, 3);
        assertThrowsExactly(UnsupportedOperationException.class,
                () -> covidAggregationService.calculateMinMax("unknown", Collections.singletonList("RU"), dateFrom, dateTo));
    }

    @Test
    public void whenEmptyDataThenException() {
        var dateFrom = LocalDate.of(2000, 1, 2);
        var dateTo = LocalDate.of(2000, 1, 3);
        assertThrowsExactly(NoDataException.class,
                () -> covidAggregationService.calculateMinMax("min", Collections.singletonList("RU"), dateFrom, dateTo));
    }
}
