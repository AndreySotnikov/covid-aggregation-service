package com.interview.covid.service;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import static com.interview.covid.util.TestUtils.generateTestData;
import static org.junit.jupiter.api.Assertions.*;

public class AggregatedOperationTest {

    @Test
    public void whenOperationIsCorrectThenGetEnumValue() {
        assertEquals(AggregationOperation.MIN, AggregationOperation.getByOperationName("min"));
        assertEquals(AggregationOperation.MAX, AggregationOperation.getByOperationName("max"));
    }

    @Test
    public void whenOperationIsUnknownThenThrowException() {
        assertThrowsExactly(UnsupportedOperationException.class,
                () -> AggregationOperation.getByOperationName("unknownOperation"));
    }

    @Test
    public void whenOperationIsDefinedThenCalculateAggregatedValue() {
        var source = Map.of(
                "RU", List.of(
                        generateTestData(10L, 2020, 1, 1),
                        generateTestData(12L, 2020, 1, 2),
                        generateTestData(14L, 2020, 1, 3),
                        generateTestData(2L, 2020, 1, 4),
                        generateTestData(23L, 2020, 1, 5)),
                "US", List.of(
                        generateTestData(11L, 2020, 1, 1),
                        generateTestData(13L, 2020, 1, 2),
                        generateTestData(17L, 2020, 1, 3),
                        generateTestData(1L, 2020, 1, 4),
                        generateTestData(21L, 2020, 1, 5)),
                "AM", List.of(
                        generateTestData(6L, 2020, 1, 1),
                        generateTestData(7L, 2020, 1, 2),
                        generateTestData(10L, 2020, 1, 3),
                        generateTestData(12L, 2020, 1, 4),
                        generateTestData(5L, 2020, 1, 5))
        );

        var resultMin = AggregationOperation.MIN.doAggregation(source);
        assertNotNull(resultMin);
        assertEquals(1L, resultMin.count());
        assertEquals(LocalDate.of(2020, 1, 4), resultMin.date());
        assertEquals("US", resultMin.country());

        var resultMax = AggregationOperation.MAX.doAggregation(source);
        assertNotNull(resultMax);
        assertEquals(23L, resultMax.count());
        assertEquals(LocalDate.of(2020, 1, 5), resultMax.date());
        assertEquals("RU", resultMax.country());
    }

}
