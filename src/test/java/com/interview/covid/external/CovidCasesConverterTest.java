package com.interview.covid.external;

import com.interview.covid.external.model.DailyCases;
import com.interview.covid.external.util.CovidCasesConverter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;

import static com.interview.covid.util.TestUtils.generateTestData;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class CovidCasesConverterTest {

    private final CovidCasesConverter covidCasesConverter;

    @Autowired
    public CovidCasesConverterTest(CovidCasesConverter covidCasesConverter) {
        this.covidCasesConverter = covidCasesConverter;
    }

    @Test
    public void whenEmptyDataThenEmptyResult() {
        var result = covidCasesConverter.convertTotalDailyCasesToNewDaily(Collections.emptyList());
        assertEquals(Collections.emptyList(), result);
    }
    @Test
    public void whenNullDataThenEmptyResult() {
        var result = covidCasesConverter.convertTotalDailyCasesToNewDaily(null);
        assertEquals(Collections.emptyList(), result);
    }

    @Test
    public void whenCorrectAccumulatedDataThenCorrectResult() {
        var source = Arrays.asList(
                generateTestData(10L, 2020, 1, 1),
                generateTestData(11L, 2020, 1, 2),
                generateTestData(15L, 2020, 1, 3),
                generateTestData(21L, 2020, 1, 4),
                generateTestData(35L, 2020, 1, 5));
        var expectedNewCases = new Long[]{10L, 1L, 4L, 6L, 14L};

        var result = covidCasesConverter.convertTotalDailyCasesToNewDaily(source);
        assertEquals(5, result.size());

        for (int i = 0; i < result.size(); i++) {
            DailyCases currentDateCases = result.get(i);
            LocalDate date = currentDateCases.date();
            assertNotNull(date);
            assertEquals(2020, date.getYear(), "Index " + i + " year error");
            assertEquals(1, date.getMonthValue(), "Index " + i + " month error");
            assertEquals(i + 1, date.getDayOfMonth(), "Index " + i + " day error");
            assertEquals(expectedNewCases[i], currentDateCases.casesCount(), "Index " + i + " cases error");
        }

    }

}
