package com.interview.covid.external.covid19api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.interview.covid.external.covid19api.dto.Covid19ApiCountry;
import com.interview.covid.external.covid19api.dto.Covid19ApiCountryTotalPerDay;
import com.interview.covid.external.covid19api.mapping.Covid19ApiMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class Covid19ApiMapperTest {

    private final Covid19ApiMapper covid19ApiMapper;
    private final ObjectMapper objectMapper;

    @Autowired
    public Covid19ApiMapperTest(Covid19ApiMapper covid19ApiMapper, ObjectMapper objectMapper) {
        this.covid19ApiMapper = covid19ApiMapper;
        this.objectMapper = objectMapper;
    }

    @Test
    public void whenNullCasesThenNull() throws JsonProcessingException {
        var source = "null";
        var parsedJsonValue = objectMapper.readValue(source, Covid19ApiCountryTotalPerDay.class);
        assertNull(parsedJsonValue);
        var result = covid19ApiMapper.mapConfirmed(parsedJsonValue);
        assertNull(result);
    }

    @Test
    public void whenNullCountryThenNull() throws JsonProcessingException {
        var source = "null";
        var parsedJsonValue = objectMapper.readValue(source, Covid19ApiCountry.class);
        assertNull(parsedJsonValue);
        var result = covid19ApiMapper.mapCountry(parsedJsonValue);
        assertNull(result);
    }

    @Test
    public void whenCorrectCasesSourceValueThenCorrectResult() throws JsonProcessingException {
        var source = """
                {
                    "Country": "United States of America",
                    "CountryCode": "",
                    "Province": "",
                    "City": "",
                    "CityCode": "",
                    "Lat": "0",
                    "Lon": "0",
                    "Confirmed": 102272079,
                    "Deaths": 1107602,
                    "Recovered": 0,
                    "Active": 101173307,
                    "Date": "2023-01-28T00:00:00Z",
                    "Comment": "no reliable source to provide recovered data"
                }
                """;
        var parsedJsonValue = objectMapper.readValue(source, Covid19ApiCountryTotalPerDay.class);
        assertNotNull(parsedJsonValue);
        var result = covid19ApiMapper.mapConfirmed(parsedJsonValue);
        assertNotNull(result);
        assertEquals(102272079L, result.casesCount());
        assertEquals(LocalDate.of(2023, 1, 28), result.date());
    }

    @Test
    public void whenCorrectCountrySourceValueThenCorrectResult() throws JsonProcessingException {
        var source = """
                {
                    "Country": "Armenia",
                    "Slug": "armenia",
                    "ISO2": "AM"
                }
                """;
        var parsedJsonValue = objectMapper.readValue(source, Covid19ApiCountry.class);
        assertNotNull(parsedJsonValue);
        var result = covid19ApiMapper.mapCountry(parsedJsonValue);
        assertNotNull(result);
        assertEquals("armenia", result.apiCode());
        assertEquals("AM", result.iso2Code());
    }
}
