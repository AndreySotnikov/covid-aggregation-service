package com.interview.covid.integration;

import com.interview.covid.dto.CasesAggregationResult;
import com.interview.covid.service.CovidAggregationService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
public class IntegrationTest {

    @Autowired
    private CovidAggregationService covidAggregationService;

    @Test
    public void whenParametersAreCorrectThenGetResult() {
        var dateFrom = LocalDate.of(2022, 1, 2);
        var dateTo = LocalDate.of(2022, 1, 3);
        var countries = Collections.singletonList("RU");

        var resultMin = covidAggregationService.calculateMinMax("min", countries, dateFrom, dateTo);
        var expectedMin = new CasesAggregationResult("RU", 16193L, LocalDate.of(2022, 1, 3));

        assertNotNull(resultMin);
        assertEquals(expectedMin, resultMin);

        var resultMax = covidAggregationService.calculateMinMax("max", countries, dateFrom, dateTo);
        var expectedMax = new CasesAggregationResult("RU", 18088L, LocalDate.of(2022, 1, 2));
        assertNotNull(resultMax);
        assertEquals(expectedMax, resultMax);
    }

}
