package com.interview.covid.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.interview.covid.dto.CasesAggregationResult;
import com.interview.covid.exception.UnknownCountryException;
import com.interview.covid.external.model.Country;
import com.interview.covid.service.CovidAggregationService;
import com.interview.covid.service.cache.CountriesStorage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.Arrays;

import static org.hamcrest.core.StringContains.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CovidControllerTest {

    private static final String REQUEST_URL_MIN = "/covid/confirmed/min";
    private static final String REQUEST_URL_MAX = "/covid/confirmed/max";
    private static final String REQUEST_URL_UNSUPPORTED = "/covid/confirmed/unsupported";
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private CovidAggregationService covidAggregationService;

    @MockBean
    private CountriesStorage countriesStorage;

    @BeforeEach
    public void beforeEach() {
        when(countriesStorage.getCountryByIso2Code("RU")).thenReturn(new Country("russia", "RU"));
        when(countriesStorage.getCountryByIso2Code("US")).thenReturn(new Country("united-states", "US"));
        when(countriesStorage.getCountryByIso2Code("QQ")).thenThrow(new UnknownCountryException("Unknown country QQ"));
    }

    @Test
    public void whenAllParametersAreValidAndMinOperationThenCalculateMinValue() throws Exception {

        var startDate = LocalDate.of(2020, 1, 1);
        var endDate = LocalDate.of(2020, 12, 1);
        var expectValue = new CasesAggregationResult("RU", 10L, LocalDate.of(2020, 1, 10));

        when(covidAggregationService.calculateMinMax("min", Arrays.asList("RU", "US"), startDate, endDate))
                .thenReturn(expectValue);

        var request = get(REQUEST_URL_MIN)
                .param("countries", "RU", "US")
                .param("dateFrom", "2020-01-01")
                .param("dateTo", "2020-12-01");

        var stringValue = objectMapper.writeValueAsString(expectValue);
        this.mockMvc.perform(request).andExpectAll(status().isOk(), content().json(stringValue));
    }

    @Test
    public void whenAllParametersAreValidAndMaxOperationThenCalculateMaxValue() throws Exception {

        var startDate = LocalDate.of(2020, 1, 1);
        var endDate = LocalDate.of(2020, 12, 1);
        var expectValue = new CasesAggregationResult("US", 10L, LocalDate.of(2020, 1, 5));

        when(covidAggregationService.calculateMinMax("max", Arrays.asList("RU", "US"), startDate, endDate))
                .thenReturn(expectValue);

        var request = get(REQUEST_URL_MAX)
                .param("countries", "RU", "US")
                .param("dateFrom", "2020-01-01")
                .param("dateTo", "2020-12-01");

        var stringValue = objectMapper.writeValueAsString(expectValue);
        this.mockMvc.perform(request).andExpectAll(status().isOk(), content().json(stringValue));
    }

    @Test
    public void whenParametersAreWrongThenValidationException() throws Exception {
        var request1 = get(REQUEST_URL_MAX)
                .param("countries", "")
                .param("dateFrom", "2020-01-01")
                .param("dateTo", "2020-12-01");

        this.mockMvc.perform(request1).andExpectAll(status().isBadRequest(),
                content().string(containsString("calculateMinMaxCases.countries: countries parameter must be defined")));

        var request2 = get(REQUEST_URL_MAX)
                .param("countries", "QQ")
                .param("dateFrom", "2020-01-01")
                .param("dateTo", "2020-12-01");

        this.mockMvc.perform(request2).andExpectAll(status().isBadRequest(),
                content().string(containsString("calculateMinMaxCases.countries: Unknown country: QQ")));

        var request3 = get(REQUEST_URL_MAX)
                .param("countries", "RU")
                .param("dateFrom", "2020-01-99")
                .param("dateTo", "2020-12-01");

        this.mockMvc.perform(request3).andExpectAll(status().isBadRequest(),
                content().string(containsString("calculateMinMaxCases.dateFromStr: dateFrom parameter must be is ISO format")));

        var request4 = get(REQUEST_URL_MAX)
                .param("countries", "")
                .param("dateFrom", "2020-01-01")
                .param("dateTo", "qqqq");

        this.mockMvc.perform(request4).andExpectAll(status().isBadRequest(),
                content().string(containsString("calculateMinMaxCases.dateToStr: dateTo parameter must be is ISO format")));

        var request5 = get(REQUEST_URL_UNSUPPORTED)
                .param("countries", "RU")
                .param("dateFrom", "2020-01-01")
                .param("dateTo", "2020-12-01");

        this.mockMvc.perform(request5).andExpectAll(status().isBadRequest(),
                content().string(containsString("calculateMinMaxCases.operation: Only min/max operations are supported")));
    }


}
