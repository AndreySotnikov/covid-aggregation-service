package com.interview.covid.util;

import com.interview.covid.external.model.DailyCases;

import java.time.LocalDate;

public class TestUtils {
    public static DailyCases generateTestData(Long count, int year, int month, int dayOfMonth) {
        return new DailyCases(LocalDate.of(year, month, dayOfMonth), count);
    }
}
