package com.interview.covid.service.cache;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.util.concurrent.UncheckedExecutionException;
import com.interview.covid.exception.ExternalApiException;
import com.interview.covid.external.CovidDataProvider;
import com.interview.covid.external.model.CountryCases;
import com.interview.covid.external.model.DailyCases;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author Andrei Sotnikov
 */
@Component
public class CovidCasesCache {

    private static final Logger LOGGER = LoggerFactory.getLogger(CovidCasesCache.class);
    private final LoadingCache<String, List<DailyCases>> confirmedCasesCache;
    public CovidCasesCache(CovidDataProvider covidDataProvider) {
        confirmedCasesCache = CacheBuilder.newBuilder()
                .build(new CacheLoader<>() {
                    @Override
                    public List<DailyCases> load(String key) {
                        var cases = covidDataProvider.findConfirmedCasesByCountry(key).cases();
                        LOGGER.debug("Loaded data for the country: " + key);
                        return cases;
                    }
                });
    }

    @Scheduled(cron = "${app.storageRefreshCron}")
    public void invalidateCache() {
        confirmedCasesCache.invalidateAll();
        LOGGER.debug("Invalidated cases cache");
    }

    public CountryCases findCachedConfirmedCasesByCountry(String country) {
        try {
            List<DailyCases> cases = confirmedCasesCache.getUnchecked(country);
            return new CountryCases(country, cases);
        } catch (UncheckedExecutionException e) {
            throw new ExternalApiException(e.getCause());
        }
    }
}

