package com.interview.covid.service.cache;

import com.interview.covid.exception.UnknownCountryException;
import com.interview.covid.external.CovidDataProvider;
import com.interview.covid.external.model.Country;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @author Andrei Sotnikov
 */
@Component
public class CountriesStorage {
    private final Map<String, Country> countriesByIso2CodeStorage;
    private final Map<String, Country> countriesByApiCodeStorage;
    private final CovidDataProvider covidDataProvider;
    private final ReadWriteLock lock;

    public CountriesStorage(CovidDataProvider covidDataProvider) {
        this.covidDataProvider = covidDataProvider;
        this.countriesByIso2CodeStorage = new HashMap<>();
        this.countriesByApiCodeStorage = new HashMap<>();
        this.lock = new ReentrantReadWriteLock();
    }

    @PostConstruct
    private void init() {
        refreshData();
    }

    public void refreshData() {
        var writeLock = lock.writeLock();
        try {
            writeLock.lock();
            covidDataProvider.findSupportedCountries().forEach(c -> {
                countriesByIso2CodeStorage.put(c.iso2Code(), c);
                countriesByApiCodeStorage.put(c.apiCode(), c);
            });
        } finally {
            writeLock.unlock();
        }
    }

    public Country getCountryByIso2Code(String iso2Code) {
        Lock readLock = lock.readLock();
        try {
            readLock.lock();
            return Optional.ofNullable(countriesByIso2CodeStorage.get(iso2Code.toUpperCase()))
                    .orElseThrow(() -> new UnknownCountryException("Country with code " + iso2Code + " doesn't exists"));
        } finally {
            readLock.unlock();
        }
    }

    public Country getCountryByApiCode(String apiCode) {
        Lock readLock = lock.readLock();
        try {
            readLock.lock();
            return Optional.ofNullable(countriesByApiCodeStorage.get(apiCode))
                    .orElseThrow(() -> new UnknownCountryException("Country with code " + apiCode + " doesn't exists"));
        } finally {
            readLock.unlock();
        }
    }

}
