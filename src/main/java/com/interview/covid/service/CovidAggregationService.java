package com.interview.covid.service;

import com.interview.covid.dto.CasesAggregationResult;
import com.interview.covid.external.model.Country;
import com.interview.covid.external.model.CountryCases;
import com.interview.covid.external.model.DailyCases;
import com.interview.covid.service.cache.CountriesStorage;
import com.interview.covid.service.cache.CovidCasesCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Andrei Sotnikov
 */
@Service
public class CovidAggregationService {
    private final CovidCasesCache covidCasesCache;
    private final CountriesStorage countriesStorage;

    @Autowired
    public CovidAggregationService(CovidCasesCache covidCasesCache, CountriesStorage countriesStorage) {
        this.covidCasesCache = covidCasesCache;
        this.countriesStorage = countriesStorage;
    }

    public CasesAggregationResult calculateMinMax(String operationName, List<String> countries, LocalDate dateFrom, LocalDate dateTo) {
            var countriesData = countries.parallelStream()
                    .map(countriesStorage::getCountryByIso2Code)
                    .map(Country::apiCode)
                    .map(covidCasesCache::findCachedConfirmedCasesByCountry)
                    .collect(Collectors.toMap(this::convertCountryNameToIsoCode, e -> filterCasesByDateRange(e.cases(), dateFrom, dateTo)));

            var aggregationOperation = AggregationOperation.getByOperationName(operationName);
            return aggregationOperation.doAggregation(countriesData);
    }

    private String convertCountryNameToIsoCode(CountryCases e) {
        return countriesStorage.getCountryByApiCode(e.country()).iso2Code();
    }

    private List<DailyCases> filterCasesByDateRange(List<DailyCases> source, LocalDate dateFrom, LocalDate dateTo) {
        return source.stream().filter(item -> !item.date().isBefore(dateFrom) && !item.date().isAfter(dateTo)).toList();
    }

}
