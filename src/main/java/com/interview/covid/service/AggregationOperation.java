package com.interview.covid.service;

import com.interview.covid.dto.CasesAggregationResult;
import com.interview.covid.exception.NoDataException;
import com.interview.covid.external.model.DailyCases;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Andrei Sotnikov
 */
public enum AggregationOperation {
    MIN("min") {
        @Override
        public CasesAggregationResult doAggregation(Map<String, List<DailyCases>> source) {
            Map<String, DailyCases> minPerCountry = source.entrySet().stream()
                    .collect(Collectors.toMap(Map.Entry::getKey, e -> doAggregationPerCountry(e.getValue())));
            return minPerCountry.entrySet().stream()
                    .min(Comparator.comparing(e -> e.getValue().casesCount()))
                    .map(e -> new CasesAggregationResult(e.getKey(), e.getValue().casesCount(), e.getValue().date()))
                    .orElse(null);
        }

        private DailyCases doAggregationPerCountry(List<DailyCases> source) {
            if (source.isEmpty()) {
                throw new NoDataException("No data for specified range");
            }
            var minCases = Long.MAX_VALUE;
            DailyCases result = null;
            for (DailyCases dailyCases : source) {
                if (dailyCases.casesCount() < minCases) {
                    result = dailyCases;
                    minCases = dailyCases.casesCount();
                }
            }
            return result;
        }
    },
    MAX("max") {
        @Override
        public CasesAggregationResult doAggregation(Map<String, List<DailyCases>> source) {
            Map<String, DailyCases> maxPerCountry = source.entrySet().stream()
                    .collect(Collectors.toMap(Map.Entry::getKey, e -> doAggregationPerCountry(e.getValue())));
            return maxPerCountry.entrySet().stream()
                    .max(Comparator.comparing(e -> e.getValue().casesCount()))
                    .map(e -> new CasesAggregationResult(e.getKey(), e.getValue().casesCount(), e.getValue().date()))
                    .orElse(null);
        }

        private DailyCases doAggregationPerCountry(List<DailyCases> source) {
            if (source.isEmpty()) {
                throw new NoDataException("No data for specified range");
            }
            var maxCases = Long.MIN_VALUE;
            DailyCases result = null;
            for (DailyCases dailyCases : source) {
                if (dailyCases.casesCount() > maxCases) {
                    result = dailyCases;
                    maxCases = dailyCases.casesCount();
                }
            }
            return result;
        }
    };
    private String operation;
    private static Map<String, AggregationOperation> CACHE = new HashMap<>();
    static {
        for (AggregationOperation item : values()) {
            CACHE.put(item.getOperation(), item);
        }
    }

    AggregationOperation(String operation) {
        this.operation = operation;
    }

    public CasesAggregationResult doAggregation(Map<String, List<DailyCases>> source) {
        throw new UnsupportedOperationException();
    }

    public String getOperation() {
        return operation;
    }

    public static AggregationOperation getByOperationName(String operationName) {
        var value = CACHE.get(operationName);
        if (value == null) {
            throw new UnsupportedOperationException("Operation " + operationName + " isn't supported");
        }
        return value;
    }

}
