package com.interview.covid.exception;

/**
 * @author Andrei Sotnikov
 */
public class NoDataException extends RuntimeException {
    public NoDataException(String message) {
        super(message);
    }
}
