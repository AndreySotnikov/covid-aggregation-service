package com.interview.covid.exception;

/**
 * @author Andrei Sotnikov
 */
public class TooManyApiRequestsException extends RuntimeException {

    public TooManyApiRequestsException(String message) {
        super(message);
    }
}
