package com.interview.covid.exception;

/**
 * @author Andrei Sotnikov
 */
public class UnknownCountryException extends RuntimeException {

    public UnknownCountryException(String message) {
        super(message);
    }

}
