package com.interview.covid.exception;

/**
 * @author Andrei Sotnikov
 */
public class ExternalApiException extends RuntimeException {

    public ExternalApiException(Throwable cause) {
        super(cause);
    }

}
