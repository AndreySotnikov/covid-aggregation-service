package com.interview.covid.external.model;

import java.util.List;

/**
 * @author Andrei Sotnikov
 */
public record CountryCases(String country, List<DailyCases> cases) {
}
