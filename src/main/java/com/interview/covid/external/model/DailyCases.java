package com.interview.covid.external.model;

import java.time.LocalDate;

/**
 * @author Andrei Sotnikov
 */
public record DailyCases(LocalDate date, Long casesCount) {
}
