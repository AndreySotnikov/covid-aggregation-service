package com.interview.covid.external.model;

/**
 * @author Andrei Sotnikov
 */
public record Country(String apiCode, String iso2Code) {
}
