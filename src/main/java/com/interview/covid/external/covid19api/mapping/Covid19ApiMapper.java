package com.interview.covid.external.covid19api.mapping;

import com.interview.covid.external.covid19api.dto.Covid19ApiCountry;
import com.interview.covid.external.covid19api.dto.Covid19ApiCountryTotalPerDay;
import com.interview.covid.external.model.Country;
import com.interview.covid.external.model.DailyCases;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * @author Andrei Sotnikov
 */
@Mapper(componentModel = "spring")
public interface Covid19ApiMapper {

    @Mappings({
            @Mapping(source = "confirmed", target = "casesCount")
    })
    DailyCases mapConfirmed(Covid19ApiCountryTotalPerDay source);

    @Mappings({
        @Mapping(source = "slug", target = "apiCode"),
        @Mapping(source = "iso2", target = "iso2Code")
    })
    Country mapCountry(Covid19ApiCountry source);

}
