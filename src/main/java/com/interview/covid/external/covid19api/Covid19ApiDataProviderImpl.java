package com.interview.covid.external.covid19api;

import com.google.common.util.concurrent.RateLimiter;
import com.interview.covid.exception.ExternalApiException;
import com.interview.covid.exception.TooManyApiRequestsException;
import com.interview.covid.exception.UnknownCountryException;
import com.interview.covid.external.CovidDataProvider;
import com.interview.covid.external.covid19api.configuration.Covid19ApiConfigurationProperties;
import com.interview.covid.external.covid19api.dto.Covid19ApiCountry;
import com.interview.covid.external.covid19api.dto.Covid19ApiCountryTotalPerDay;
import com.interview.covid.external.covid19api.mapping.Covid19ApiMapper;
import com.interview.covid.external.model.Country;
import com.interview.covid.external.model.CountryCases;
import com.interview.covid.external.util.CovidCasesConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

/**
 * @author Andrei Sotnikov
 */
@Service
@SuppressWarnings("UnstableApiUsage")
public class Covid19ApiDataProviderImpl implements CovidDataProvider {

    private static final double DEFAULT_RATE_LIMIT = 0.1;

    private final RestTemplate restTemplate;
    private final Covid19ApiMapper covid19ApiMapper;
    private final CovidCasesConverter covidCasesConverter;
    private final Covid19ApiConfigurationProperties covid19ApiConfigurationProperties;
    private final RateLimiter rateLimiter;

    @Autowired
    public Covid19ApiDataProviderImpl(RestTemplate restTemplate, Covid19ApiMapper covid19ApiMapper,
                                      CovidCasesConverter covidCasesConverter, Covid19ApiConfigurationProperties covid19ApiConfigurationProperties) {
        this.restTemplate = restTemplate;
        this.covid19ApiMapper = covid19ApiMapper;
        this.covidCasesConverter = covidCasesConverter;
        this.covid19ApiConfigurationProperties = covid19ApiConfigurationProperties;
        this.rateLimiter = RateLimiter.create(getInitialRateLimit());
    }

    @Override
    public CountryCases findConfirmedCasesByCountry(String country) {
        var serverData = findConfirmedCasesFromServer(country);
        var dailyTotalCases = Arrays.stream(serverData)
                .map(covid19ApiMapper::mapConfirmed)
                .toList();
        return new CountryCases(country, covidCasesConverter.convertTotalDailyCasesToNewDaily(dailyTotalCases));
    }

    @Override
    public List<Country> findSupportedCountries() {
        var serverData = findCountriesFromServer();
        return Arrays.stream(serverData)
                .map(covid19ApiMapper::mapCountry)
                .toList();
    }

    private Covid19ApiCountry[] findCountriesFromServer() {
        try {
            rateLimiter.acquire(isRateLimitEnabled() ? 1 : 0);
            return restTemplate.getForObject(covid19ApiConfigurationProperties.getBaseUrl() + "/countries", Covid19ApiCountry[].class);
        } catch (HttpClientErrorException.TooManyRequests e) {
            throw new TooManyApiRequestsException("Too many requests to covid19api. Try again.");
        } catch (Exception e) {
            throw new ExternalApiException(e);
        }
    }

    private Covid19ApiCountryTotalPerDay[] findConfirmedCasesFromServer(String country) {
        try {
            rateLimiter.acquire(isRateLimitEnabled() ? 1 : 0);
            return restTemplate.getForObject(covid19ApiConfigurationProperties.getBaseUrl() + "/total/country/" + country, Covid19ApiCountryTotalPerDay[].class);
        } catch (HttpClientErrorException.NotFound e) {
            throw new UnknownCountryException("Unknown country: " + country);
        } catch (HttpClientErrorException.TooManyRequests e) {
            throw new TooManyApiRequestsException("Too many requests to covid19api. Try again.");
        } catch (Exception e) {
            throw new ExternalApiException(e);
        }
    }

    private boolean isRateLimitEnabled() {
        var configRateLimit = covid19ApiConfigurationProperties.getRateLimit();
        return configRateLimit != null && configRateLimit > 0;
    }
    private double getInitialRateLimit() {
        var configRateLimit = covid19ApiConfigurationProperties.getRateLimit();
        return isRateLimitEnabled() ? configRateLimit: DEFAULT_RATE_LIMIT;
    }
}
