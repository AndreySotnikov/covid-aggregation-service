package com.interview.covid.external.covid19api.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author Andrei Sotnikov
 */
@Configuration
@ConfigurationProperties(prefix = "covid19api")
public class Covid19ApiConfigurationProperties {
    private String baseUrl;
    private Double rateLimit;


    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public Double getRateLimit() {
        return rateLimit;
    }

    public void setRateLimit(Double rateLimit) {
        this.rateLimit = rateLimit;
    }

}
