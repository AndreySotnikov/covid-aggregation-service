package com.interview.covid.external.covid19api.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;

/**
 * @author Andrei Sotnikov
 */
public record Covid19ApiCountryTotalPerDay(
        @JsonProperty("Country") String country,
        @JsonProperty("CountryCode") String countryCode,
        @JsonProperty("Province") String province,
        @JsonProperty("City") String city,
        @JsonProperty("CityCode") String cityCode,
        @JsonProperty("Lat") String lat,
        @JsonProperty("Lon") String lon,
        @JsonProperty("Confirmed") Long confirmed,
        @JsonProperty("Recovered") Long recovered,
        @JsonProperty("Active") Long active,
        @JsonProperty("Date") @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'") LocalDate date,
        @JsonProperty("Comment") String comment) {
}
