package com.interview.covid.external.covid19api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author Andrei Sotnikov
 */
public record Covid19ApiCountry(@JsonProperty("Country") String country,
                                @JsonProperty("Slug") String slug,
                                @JsonProperty("ISO2") String iso2) {
}
