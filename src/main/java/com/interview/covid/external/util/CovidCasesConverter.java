package com.interview.covid.external.util;

import com.interview.covid.external.model.DailyCases;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Andrei Sotnikov
 */
@Component
public class CovidCasesConverter {

    public List<DailyCases> convertTotalDailyCasesToNewDaily(List<DailyCases> dailyTotalCases) {
        if (dailyTotalCases == null) {
            return Collections.emptyList();
        }
        var dailyConfirmedCases = new ArrayList<DailyCases>();
        if (!dailyTotalCases.isEmpty()) {
            var firstDay = dailyTotalCases.get(0);
            dailyConfirmedCases.add(new DailyCases(firstDay.date(), firstDay.casesCount()));
        }
        for (int i = 1; i < dailyTotalCases.size(); i++) {
            var currentDay = dailyTotalCases.get(i);
            var currentDayCount = currentDay.casesCount();
            var previousDayCount = dailyTotalCases.get(i - 1).casesCount();
            dailyConfirmedCases.add(new DailyCases(currentDay.date(), currentDayCount - previousDayCount));
        }
        return dailyConfirmedCases;
    }

}
