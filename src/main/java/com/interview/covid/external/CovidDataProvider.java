package com.interview.covid.external;

import com.interview.covid.external.model.Country;
import com.interview.covid.external.model.CountryCases;

import java.util.List;

/**
 * @author Andrei Sotnikov
 */
public interface CovidDataProvider {

    CountryCases findConfirmedCasesByCountry(String country);

    List<Country> findSupportedCountries();

}
