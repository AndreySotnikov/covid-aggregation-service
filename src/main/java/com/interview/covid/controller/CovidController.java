package com.interview.covid.controller;

import com.interview.covid.controller.validation.ValidCountries;
import com.interview.covid.dto.CasesAggregationResult;
import com.interview.covid.service.CovidAggregationService;
import jakarta.validation.constraints.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * @author Andrei Sotnikov
 */
@Validated
@RestController
@RequestMapping("covid")
public class CovidController {
    private static final Logger LOGGER = LoggerFactory.getLogger(CovidController.class);
    private static final String DATE_REGEXP = "^([0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9])$";
    private final CovidAggregationService covidAggregationService;

    @Autowired
    public CovidController(CovidAggregationService covidAggregationService) {
        this.covidAggregationService = covidAggregationService;
    }

    @GetMapping("/confirmed/{operation}")
    public CasesAggregationResult calculateMinMaxCases(
            @PathVariable("operation") @Pattern(regexp = "^(min|max)$", message = "Only min/max operations are supported") String operation,
            @RequestParam("countries") @ValidCountries List<String> countries,
            @RequestParam("dateFrom") @Pattern(regexp = DATE_REGEXP, message = "dateFrom parameter must be is ISO format") String dateFromStr,
            @RequestParam("dateTo") @Pattern(regexp = DATE_REGEXP, message = "dateTo parameter must be is ISO format") String dateToStr) {
        LOGGER.debug("MinMax calculation parameters: Operation - {}, Countries - {} DateFrom - {}, DateTo - {}",
                operation, countries, dateFromStr, dateToStr);

        LocalDate dateFrom = DateTimeFormatter.ISO_DATE.parse(dateFromStr, LocalDate::from);
        LocalDate dateTo = DateTimeFormatter.ISO_DATE.parse(dateToStr, LocalDate::from);
        var response = covidAggregationService.calculateMinMax(operation, countries, dateFrom, dateTo);

        LOGGER.debug("MinMax calculation response: {}", response);
        return response;
    }

}
