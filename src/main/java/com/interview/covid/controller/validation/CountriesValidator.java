package com.interview.covid.controller.validation;

import com.interview.covid.exception.UnknownCountryException;
import com.interview.covid.service.cache.CountriesStorage;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author Andrei Sotnikov
 */
public class CountriesValidator implements ConstraintValidator<ValidCountries, List<String>> {

    private final CountriesStorage countriesStorage;

    private String message;

    @Autowired
    public CountriesValidator(CountriesStorage countriesStorage) {
        this.countriesStorage = countriesStorage;
    }

    @Override
    public void initialize(ValidCountries constraintAnnotation) {
        this.message = constraintAnnotation.message();
    }

    @Override
    public boolean isValid(List<String> value, ConstraintValidatorContext context) {
        if (value.isEmpty()) {
            context.disableDefaultConstraintViolation();
            context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
            return false;
        }
        for (String item : value) {
            try {
                countriesStorage.getCountryByIso2Code(item);
            } catch (UnknownCountryException e) {
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate("Unknown country: " + item).addConstraintViolation();
                return false;
            }
        }

        return true;
    }
}
