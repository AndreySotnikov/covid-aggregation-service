package com.interview.covid.controller.validation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author Andrei Sotnikov
 */
@Documented
@Constraint(validatedBy = CountriesValidator.class)
@Target({FIELD, ANNOTATION_TYPE, PARAMETER})
@Retention(RUNTIME)
public @interface ValidCountries {
    String message() default "countries parameter must be defined";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
