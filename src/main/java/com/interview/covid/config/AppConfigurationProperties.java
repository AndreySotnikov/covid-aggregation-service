package com.interview.covid.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "app")
public class AppConfigurationProperties {
    private String storageRefreshCron;

    public String getStorageRefreshCron() {
        return storageRefreshCron;
    }

    public void setStorageRefreshCron(String storageRefreshCron) {
        this.storageRefreshCron = storageRefreshCron;
    }
}
