package com.interview.covid.dto;

/**
 * @author Andrei Sotnikov
 */
public record ErrorResult(int code, String message) {
}
