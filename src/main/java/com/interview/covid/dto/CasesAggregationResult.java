package com.interview.covid.dto;

import java.time.LocalDate;

/**
 * @author Andrei Sotnikov
 */
public record CasesAggregationResult(String country, Long count, LocalDate date) {
}
